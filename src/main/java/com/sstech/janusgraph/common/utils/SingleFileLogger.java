package com.sstech.janusgraph.common.utils;

import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class SingleFileLogger {
    private static SingleFileLogger instance = null;
    private static Logger logger;
    private static FileHandler fh;
    private static Formatter sf;

    private SingleFileLogger(String fileName)
    {
        try {
            fh = new FileHandler(fileName, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger = Logger.getLogger("LogMe");

        sf = new SimpleFormatter();
        fh.setFormatter(sf);
        logger.addHandler(fh);
        logger.setUseParentHandlers(false);

        //Part of making this class a singleton

        instance = this;
    }

    public static SingleFileLogger getInstance(String filename)
    {
        if (instance == null)
            instance = new SingleFileLogger(filename);

        return instance;
    }
//
//    public LogMe() {
//        //Make this class a singleton
//        if (logMe != null) {
//            return;
//        }
//
//        //Create the log file
//
//
//        sf = new SimpleFormatter();
//        fh.setFormatter(sf);
//        logger.addHandler(fh);
//
//        //Part of making this class a singleton
//        logger = Logger.getLogger("LogMe");
//        logMe = this;
//    }

    public Logger getLogger() {
        return SingleFileLogger.logger;
    }
}
