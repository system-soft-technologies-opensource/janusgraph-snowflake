package com.sstech.janusgraph.common.exceptions;

public class SnowFlakeConnectionException extends Exception {
    public SnowFlakeConnectionException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}