package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.utils.JanusGraphSnowFlakeLogger;
//import com.sstech.janusgraph.common.utils.LogMe;
import com.sstech.janusgraph.common.utils.LogUtils;
import net.snowflake.client.jdbc.SnowflakeSQLException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class SnowFlakeCreateStatement {

    private Connection connection;
    private Statement statement;

    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeCreateStatement.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;

    public SnowFlakeCreateStatement(Connection snowFlakeConnection) throws SQLException, IOException {
        connection = snowFlakeConnection;
        statement = connection.createStatement();

        try {
            log = logger.getLogger();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
        }
//        log = logger.getLogger();
    }

    public int executeUpdate(String sql) throws SQLException {
        log.info(LogUtils.stringFormatter("Successfully executed update %s from cursor", sql));
        return statement.executeUpdate(sql);
    }

    public ResultSet executeQuery(String sql) throws SQLException {
//        ResultSet result = null;
        log.info("Executing SQL query " + sql + " from cursor");
        try{
            ResultSet result = statement.executeQuery(sql);
            log.info(LogUtils.stringFormatter("Successfully executed query %s from cursor", sql));
            return result;
        }
        catch (SnowflakeSQLException e) {
            log.severe("Unable to run query " + sql + " from cursor");
            throw new SQLException("Unable to run the query " + sql + " Is statement null? " + String.valueOf(statement == null) + String.valueOf(connection == null));
        }
    }

    public void close() throws SQLException {
        try {
            statement.close();
            log.info("Successfully closed connection for statement object in cursor");
            connection.close();
            log.info("Successfully closed connection for JDBC connection object in cursor");
        }
        catch (SQLException e) {
            log.severe("Unable to close connection to SnowFlake from cursor");
        }
    }
}
