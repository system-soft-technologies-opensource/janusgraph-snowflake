// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.snowflake.snowflakedb;

// This is the helper class to provide a easy to use helper reference for doing SnowFlake based functionalities
// graph = JanusGraphFactory.open("conf/janusgraph-snowflake.properties")

import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.JanusGraphSnowFlakeLogger;
//import com.sstech.janusgraph.common.utils.LogMe;
import com.sstech.janusgraph.common.utils.LogUtils;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;

public class SnowFlakeDatabase {
    private String tableName;
    private SnowFlakeTransaction transaction;
    private SnowFlakeConnection connection;
    private boolean KEYEXIST = false;

    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeDatabase.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;

    public  SnowFlakeDatabase(String name, SnowFlakeTransaction tx) throws IOException, SnowFlakeConnectionException {
        transaction = tx;
        tableName = name;
        connection = transaction.getConnection();

        try {
            log = logger.getLogger();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
        }
//        log = logger.getLogger();
    }

    public SnowFlakeConnection getConnection() {
        return connection;
    }
//
//    private static Connection getSnowflakeConnection()
//            throws SQLException {
//
//        // build connection properties
//        Properties properties = new Properties();
//        properties.put("user", "Debasish");        // replace "" with your user name
//        properties.put("password", "Nano789!");    // replace "" with your password
//        properties.put("warehouse", "COMPUTE_WH");   // replace "" with target warehouse name
//        properties.put("db", "GRAPHDBTEST");          // replace "" with target database name
//        properties.put("schema", "GRAPHDBTEST1");      // replace "" with target schema name
//        //properties.put("tracing", "on"); // optional tracing property
//
//        // Replace <account> with your account, as provided by Snowflake.
//        // Replace <region_id> with the name of the region where your account is located.
//        // If your platform is AWS and your region ID is US West, you can omit the region ID segment.
//        // Replace <platform> with your platform, for example "azure".
//        // If your platform is AWS, you may omit the platform.
//        // Note that if you omit the region ID or the platform, you should also omit the
//        // corresponding "."  E.g. if your platform is AWS and your region is US West, then your
//        // connectStr will look similar to:
//        // "jdbc:snowflake://xy12345.snowflakecomputing.com";
//        String connectStr = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com";
//        return DriverManager.getConnection(connectStr, properties);
//    }

    public OperationStatus put(StaticBuffer key, StaticBuffer value) {
        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error

        String statement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                "put", tableName, key.toString(), value.toString(), statement));

        return connection.executeUpdate(statement);
    }

    public OperationStatus put(byte[] key, byte[] value) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error
        String insertStatement = "INSERT INTO " + tableName
                + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                "put(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

        return connection.executeUpdate(insertStatement);
    }

    public OperationStatus putNotOverWrite(StaticBuffer key, StaticBuffer value) {
        String statement = "SELECT COUNT(*) FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64')";
        if ((Integer) connection.executeQuery(statement).getResult() > 0) {
            String insertStatement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;
            return connection.executeUpdate(insertStatement);
        }
        else {
            OperationStatus status = new OperationStatus(false);
            status.setKEYEXIST(true);
            return status;
        }
    }

    public OperationStatus putNotOverWrite(byte[] key, byte[] value) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

        String statement = "SELECT COUNT(*) FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

        if ((Integer) connection.executeQuery(statement).getResult() == 0) {
            String insertStatement = "INSERT INTO " + tableName
                    + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
                    "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

            log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
                    "putNotOverWrite(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

            return connection.executeUpdate(insertStatement);
        }
        else {
            OperationStatus status = new OperationStatus(false);
            status.setKEYEXIST(true);
            log.severe(LogUtils.stringFormatter("Unable to put key=%s & value=%s and key=%s exists", keyBase64Encoded, valueBase64Encoded, keyBase64Encoded));
            return status;
        }
    }

    public OperationStatus delete(StaticBuffer key) {

        String statement = "TRUNCATE * FROM " + tableName + " WHERE KEY = " + key;

        return connection.executeUpdate(statement);
    }

    public OperationStatus delete(byte[] key) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

        String statement = "DELETE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "delete(encoded)", tableName, keyBase64Encoded, statement));


        return connection.executeUpdate(statement);
    }

    public OperationStatus get(StaticBuffer key) {
        String statement = "";
        return connection.executeQuery(statement);
    }

//    public String get(String key, String binaryFormat) throws SQLException
//    {
//    	 String qry = "SELECT TO_VARCHAR(KEY,'" + binaryFormat + "') FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64') limit 1";
//        // System.out.println(statement);
//         log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
//                 "get(encoded)", tableName, key, qry));
//        // Statement statement = connection.c
//        // Statement statement2 = connection.cre
//         Connection conn = getSnowflakeConnection();
//         Statement statement2 = conn.createStatement();
//         //OperationStatus status = connection.executeQuery(statement);
//
//         ResultSet result = statement2.executeQuery(qry);
//         String out = null;
//         while(result.next())
//         {
//        	 out = result.getString(1);
//         }
//
//         return out;
//    }

    public OperationStatus get(String key) throws SQLException
    {
        String qry = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64') limit 1";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "get(encoded)", tableName, key, qry));
        // Statement statement = connection.c
        // Statement statement2 = connection.cre
//        Connection conn = getSnowflakeConnection();
//        Statement statement2 = conn.createStatement();
        //OperationStatus status = connection.executeQuery(statement);

//        ResultSet result = statement2.executeQuery(qry);
//        String out = null;
//        while(result.next())
//        {
//            out = result.getString(1);
//        }

        return connection.executeQuery(qry);
    }
    
    
    
    /*public OperationStatus get(String key) {
       // String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
       // String keyBase64Encoded = key.toString();
       // System.out.println(keyBase64Encoded);
        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64')";
        System.out.println(statement);
        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "get(encoded)", tableName, key, statement));

        return connection.executeQuery(statement);
    }*/

    public OperationStatus get(byte[] key) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
       // String keyBase64Encoded = key.toString();
        System.out.println(keyBase64Encoded);
        String statement = "SELECT VALUE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";
        System.out.println(statement);
        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
                "get(encoded)", tableName, keyBase64Encoded, statement));

        return connection.executeQuery(statement);
    }

    public void close() throws PermanentBackendException {
        OperationStatus status = connection.close();
        log.info("Successfully closed connection from SnowFlake DB class");
        if (!status.getStatus()) {
            throw new PermanentBackendException("Could not close connection to Graph: " + status);
        }
    }

    public SnowFlakeCursor openCursor() throws IOException {
        SnowFlakeCursor cursor = new SnowFlakeCursor(getConnection());
        cursor.setTable(tableName);
        log.info("Successfully created cursor connection to SnowFlake using custom SnowFlakeCursor class");
        return cursor;
    }

    public OperationStatus getTables(String dbName, String schema)
    {
        //String tableName = "GRAPHDBTEST1.demo";
        try{
            String listOfTableQry = "show tables in "+dbName +"."+schema;

            OperationStatus result = connection.executeQuery(listOfTableQry);

            ResultSet resultSet = ((ResultSet) result.getResult());

            List<String> tableList = new ArrayList<String>();

            while(resultSet.next())
            {
                tableList.add(resultSet.getString("name"));
                System.out.println(resultSet.getString("name"));
                // System.out.println("row " + rowIdx + ", column 0: " +
                //                 resultSet.getString("name"));
            }
            // statement.executeUpdate(listOfTableQry);
            System.out.println(tableList.size());

            OperationStatus status = new OperationStatus(true);
            status.setResult(tableList);
            log.info("Successfully fetched list of tables as " + tableList.toString());

            return status;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    //remove tables
    public OperationStatus removeTable(String schema, String tableName)
    {
        try{
            String dropTable = "drop table if exists "+schema +"."+tableName;
//            ResultSet resultSet = connection.executeQuery(dropTable);
            OperationStatus result = connection.executeQuery(dropTable);
            ResultSet resultSet = (ResultSet) result.getResult();

            //System.out.println(" why "+result);
            while(resultSet.next())
            {
                System.out.println(resultSet.getString(1));
                return new OperationStatus(true);
            }
//            statement.close();
//            connection.close();
            // return result;

            log.info("Successfully removed table " + tableName);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            log.severe(LogUtils.logTraceback("Unable to remove table " + tableName, e));
        }
        return new OperationStatus(true);
    }

    public String getTableName() {
        return tableName;
    }
}
