package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.common.utils.JanusGraphSnowFlakeLogger;
//import com.sstech.janusgraph.common.utils.LogMe;
import com.sstech.janusgraph.common.utils.LogUtils;
import org.janusgraph.diskstorage.StaticBuffer;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.logging.Logger;

public class SnowFlakeCursor {
    private SnowFlakeConnection connection;
    private String tableName;

    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeCursor.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;

    public SnowFlakeCursor(SnowFlakeConnection snowFlakeConnection) throws IOException {
        connection = snowFlakeConnection;
        try {
            log = logger.getLogger();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
        }
//        log = logger.getLogger();
    }

    public void setTable(String table) {
        tableName = table;
    }

    public ResultSet getSearchKey(StaticBuffer key) {
        String statement = "SELECT * FROM " + tableName + " WHERE KEY = " + key.toString();

        ResultSet resultSet = connection.executeCursorQuery(statement);
        return resultSet;
    }

    public ResultSet getSearchKey(byte[] key) {
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, key=%s", "getSearchKey", tableName, key.toString()));

        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, encodedKey=%s", "getSearchKey", tableName, keyBase64Encoded));

        String statement = "SELECT * FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, sql=%s", "getSearchKey", tableName, statement));

        ResultSet resultSet = connection.executeCursorQuery(statement);

        try {
            log.info(LogUtils.stringFormatter("op=%s, tbl=%s, sql=%s, columnCount=%s", "getSearchKey", tableName, statement, String.valueOf(resultSet.getMetaData().getColumnCount())));
        } catch (SQLException e) {
            log.severe("Unable to run query " + statement);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
}
