package com.sstech.janusgraph.snowflake.connection;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.JanusGraphSnowFlakeLogger;
//import com.sstech.janusgraph.common.utils.LogMe;
import com.sstech.janusgraph.common.utils.LogUtils;
import org.janusgraph.diskstorage.PermanentBackendException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Logger;

public class SnowFlakeConnectionManager {

    private static SnowFlakeConnectionManager instance = null;

    private Integer commitTime;
    private Long startTime;
    private String URL = "";
    private Integer port = 0;
    private String userName = "";
    private String pwd = "";
    private String warehouse = "";
    private String DB = "";
    private String schema = "";
    private String role = "";
    private HashMap<String, String> extra = new HashMap<>();
    private SnowFlakeConnection connection;
    private boolean isConnected = false;

    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeConnectionManager.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;


    private SnowFlakeConnectionManager (Integer commit_time) throws PermanentBackendException {
        commitTime = commit_time;
        startTime = System.currentTimeMillis();

        try {
            log = logger.getLogger();
        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
        }
//        log = logger.getLogger();
    }

    public static SnowFlakeConnectionManager getInstance(Integer commit_time) throws PermanentBackendException {
        if (instance == null) {
            instance = new SnowFlakeConnectionManager(commit_time);
        }
        return instance;
    }

    protected void autoTimer() {

    }

    public void setURL(String path) throws SnowFlakeConnectionException {
        URL = path;
        log.info("Set JDBC URL = " + URL);
    }
    public void setPort(Integer portNumber) throws SnowFlakeConnectionException {
        port = portNumber;
        log.info("Set Port number = " + port);
    }
    public void setWarehouse(String warehouse_name) throws SnowFlakeConnectionException {
        warehouse = warehouse_name;
        log.info("Set warehouse as " + warehouse);
    }
    public void setUserName(String username) throws SnowFlakeConnectionException {
        this.userName = username;
        log.info("Set userName = " + username);
    }
    public void setDB(String DB) throws SnowFlakeConnectionException {
        this.DB = DB;
        log.info("Set DB = " + DB);
    }
    public void setSchema(String schema) throws SnowFlakeConnectionException {
        this.schema = schema;
        log.info("Set Schema = " + schema);
    }
    public void setRole(String role) throws SnowFlakeConnectionException {
        this.role = role;
        log.info("Set access role = " + role);
    }
    public void setPassword(String pwd) throws SnowFlakeConnectionException {
        this.pwd = pwd;
        log.info("Set password as = " + pwd);
    }
    public void setOthers(String propName, String propVal) throws SnowFlakeConnectionException {
        extra.putIfAbsent(propName, propVal);

//        if (areParametersSet()) {
//            connect();
//        }
    }

    public SnowFlakeConnection getConnection() throws SnowFlakeConnectionException {
        if (isConnected) {
            log.info("Connected to SnowFlake successfully");
            return connection;
        }
        else {
            throw new SnowFlakeConnectionException("All the required connection parameters " +
                    "(URL, port, warehouse) are not set before fetching connection");
        }
    }

    private boolean areParametersSet() {
        if (URL.equals("") && warehouse.equals("") && userName.equals("") && pwd.equals("") && schema.equals("")) {
            return false;
        }
        else {
            return true;
        }
    }

    public void connect()  throws SnowFlakeConnectionException {
        try {
            SnowFlakeConnection snowFlakeConnection = SnowFlakeConnection.getInstance(false);

            snowFlakeConnection.setURL(URL);
            snowFlakeConnection.setPort(port);
            snowFlakeConnection.setWarehouse(warehouse);
            snowFlakeConnection.setUserName(userName);
            snowFlakeConnection.setPassword(pwd);
            snowFlakeConnection.setDB(DB);
            snowFlakeConnection.setRole(role);
            snowFlakeConnection.setSchema(schema);

            isConnected = true;

//        snowFlakeConnection.setOthers(extra);
            connection = snowFlakeConnection.getConnection();
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw new SnowFlakeConnectionException("Couldn't connect to SnowFlake DB with properties URL: " + URL +
                    " user: " + userName + " password: " + pwd + " warehouse: " + warehouse + " role: " + role +
                    " DB: " + DB + " schema: " + schema);
        }
        catch (IOException e) {
            e.printStackTrace();
//            throw new SnowFlakeConnectionException("Unable to create log file at " + logger.getLogFileName() + " aborting connection to SnowFlake");
        }

    }

    public void close() {
        connection.close();
    }

}
