package com.sstech.janusgraph.snowflake.connection;

import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeCreateStatement;
import com.sstech.janusgraph.common.utils.JanusGraphSnowFlakeLogger;
import com.sstech.janusgraph.common.utils.LogUtils;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;
import com.jolbox.bonecp.BoneCP;


public class SnowFlakeConnection {

    private static SnowFlakeConnection instance = null;

    private String URL = "";
    private Integer port = 0;
    private String warehouse = "";
    private String extra = "";
    private String userName = "";
    private String pwd = "";
    private String schema = "";
    private String db = "";
    private String role = "";

    private String connectionURL = "";
    private Connection snowflakeJdbcConnection;
    private BoneCP connectionPool;
    private SnowFlakeCreateStatement createStatement;

    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeConnection.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;

    private boolean autoCommit = false;

    private SnowFlakeConnection(boolean auto_commit) throws IOException {

        autoCommit = auto_commit;

        try {
            log = logger.getLogger();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
        }
//        log = logger.getLogger();
    }

    public static SnowFlakeConnection getInstance(boolean auto_commit) throws IOException {
        if (instance == null) {
            instance = new SnowFlakeConnection(auto_commit);
        }
        return instance;
    }

    public void setURL(String path) {
        URL = path;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setPort(Integer portNumber) {
        port = portNumber;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setWarehouse(String warehouse_name) {
        warehouse = warehouse_name;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setOthers(String others) {
        extra = others;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setUserName(String userName) {
        this.userName = userName;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setPassword(String pwd) {
        this.pwd = pwd;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setDB(String DB) {
        this.db = DB;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setSchema(String schema) {
        this.schema = schema;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }
    public void setRole(String role) {
        this.role = role;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    private boolean areParametersSet() {
        return !URL.equals("") && port != 0 && !warehouse.equals("");
//        if (URL.equals("") && port == 0 && warehouse.equals("")) {
//            return false;
//        }
//        else {
//            return true;
//        }
    }

    private void connect() throws SQLException {
        // build connection properties
        Properties properties = new Properties();

        properties.put("user", this.userName);        // replace "" with your user name
        properties.put("password", this.pwd);    // replace "" with your password
        properties.put("warehouse", this.warehouse);   // replace "" with target warehouse name
        properties.put("db", this.db);          // replace "" with target database name
        properties.put("schema", this.schema);      // replace "" with target schema name
        properties.put("role", this.role);
        String connectStr = this.URL;
        //properties.put("tracing", "on"); // optional tracing property

//         Replace <account> with your account, as provided by Snowflake.
//         Replace <region_id> with the name of the region where your account is located.
//         If your platform is AWS and your region ID is US West, you can omit the region ID segment.
//         Replace <platform> with your platform, for example "azure".
//         If your platform is AWS, you may omit the platform.
//         Note that if you omit the region ID or the platform, you should also omit the
//         corresponding "."  E.g. if your platform is AWS and your region is US West, then your
//         connectStr will look similar to:
//
        if(connectStr == null)
        {
         connectStr = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com"; // replace accountName with your account name
        }
//        return DriverManager.getConnection(connectStr, properties);
        snowflakeJdbcConnection = DriverManager.getConnection(connectStr, properties);
//        log.info("Successfully created JDBC connection to SnowFlake");
//
//          // BoneCP
//        try {
//            BoneCPConfig config = new BoneCPConfig();
//            // jdbc url specific to your database, eg jdbc:mysql://127.0.0.1/yourdb
//            config.setJdbcUrl(connectStr);
//            config.setUsername(this.userName);
//            config.setPassword(this.pwd);
//            config.setProperties(properties);
//            config.setMinConnectionsPerPartition(5);
//            config.setMaxConnectionsPerPartition(10);
//            config.setPartitionCount(2);
//            // setup the connection pool
//            connectionPool = new BoneCP(config);
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void buildConnectionURL() {
        connectionURL = URL;
    }

    private void buildConnection() throws SQLException, IOException {
        this.connect();
//        snowflakeJdbcConnection = connectionPool.getConnection();
        createStatement = new SnowFlakeCreateStatement(snowflakeJdbcConnection);
        log.info("Successfully created 'createStatement' for JDBC connection to execute queries");
    }

    public SnowFlakeConnection getConnection() throws SQLException, IOException {
        this.buildConnection();
        return this;
    }

    public OperationStatus executeUpdate(String sql) {
        log.info("Executing SQL update " + sql);
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeUpdate(sql));
            log.info(LogUtils.stringFormatter("Successfully executed update %s", sql));
            return status;
        }
        catch (SQLException e) {
            log.severe("Unable to run update " + sql);
            return  new OperationStatus(false);
        }
    }

    public OperationStatus executeQuery(String sql) {
        log.info("Executing SQL query " + sql);
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeQuery(sql));
            log.info(LogUtils.stringFormatter("Successfully executed query %s", sql));
            return status;
        }
        catch (SQLException e) {
            log.severe("Unable to run query " + sql);
            return  new OperationStatus(false);
        }
    }

    public ResultSet executeCursorQuery(String sql) {
        log.info("Executing cursor query " + sql);
        try {
            ResultSet resultSet = createStatement.executeQuery(sql);
            log.info(LogUtils.stringFormatter("Number of columns fetched executing %s SQL is %s", sql, String.valueOf(resultSet.getMetaData().getColumnCount())));
            return resultSet;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public OperationStatus close() {
        log.info("Closing connection to SnowFlake");
        try {
            createStatement.close();
            log.info("Closed connection from createStatement");
            snowflakeJdbcConnection.close();
            log.info("Closed JDBC connection");
            return new OperationStatus(true);
        }
        catch (SQLException e) {
            log.severe("Couldn't close connection to SnowFlakeDB");
            return new OperationStatus(false);
        }
    }
}
