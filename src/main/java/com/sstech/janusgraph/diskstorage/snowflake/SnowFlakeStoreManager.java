// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



package com.sstech.janusgraph.diskstorage.snowflake;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.common.utils.*;

import com.google.common.base.Preconditions;
import org.janusgraph.diskstorage.*;
import org.janusgraph.diskstorage.BackendException;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import org.janusgraph.diskstorage.configuration.ConfigNamespace;
import org.janusgraph.diskstorage.configuration.ConfigOption;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.keycolumnvalue.KeyRange;
import org.janusgraph.diskstorage.keycolumnvalue.StandardStoreFeatures;
import org.janusgraph.diskstorage.keycolumnvalue.StoreFeatures;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVMutation;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStoreManager;
import org.janusgraph.diskstorage.util.time.TimestampProvider;
import org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


public class SnowFlakeStoreManager implements OrderedKeyValueStoreManager {

//    private static final Logger log = LoggerFactory.getLogger(SnowFlakeStoreManager.class);
    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeStoreManager.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;
    private boolean isLogged = true;

    //Review what could be put here...
    public static final ConfigNamespace SNOWFLAKE_NS =
            new ConfigNamespace(GraphDatabaseConfiguration.STORAGE_NS, "snowflake", "SnowflakeDB JE configuration options");

    private Map<String, SnowFlakeKeyValueStore> stores;
    private SnowFlakeTransaction tx;
    private Map<String, SnowFlakeDatabase> databases = new HashMap<>();

    private final ExecutorService executorService;

    //  protected Environment environment;
    protected StoreFeatures features;
    BaseTransactionConfig cfg = new BaseTransactionConfig() {
        @Override
        public Instant getCommitTime() {
            return null;
        }

        @Override
        public void setCommitTime(Instant instant) {

        }

        @Override
        public boolean hasCommitTime() {
            return false;
        }

        @Override
        public TimestampProvider getTimestampProvider() {
            return null;
        }

        @Override
        public String getGroupName() {
            return null;
        }

        @Override
        public boolean hasGroupName() {
            return false;
        }

        @Override
        public <V> V getCustomOption(ConfigOption<V> configOption) {
            return null;
        }

        @Override
        public Configuration getCustomOptions() {
            return null;
        }
    };

    public SnowFlakeStoreManager(Configuration configuration) throws BackendException {
//        super(configuration);
//        super(configuration);
        stores = new HashMap<>();

        this.executorService = new ThreadPoolExecutor(2,
                5,
                1,
                TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(),
                new ThreadFactoryBuilder()
                        .setDaemon(true)
                        .setNameFormat("SnowFlakeStoreManager[%02d]")
                        .build());

        //    int cachePercentage = configuration.get(JVM_CACHE);
        //  initialize(cachePercentage);

        try {
            log = logger.getLogger();
        } catch (IOException e) {
            System.out.println("Unable to create logger class. Not going to use logger and will do sysout");
            isLogged = false;
        }
//        log = logger.getLogger();

        features = new StandardStoreFeatures.Builder()
                .orderedScan(true)
                //.transactional(transactional)
                .keyConsistent(GraphDatabaseConfiguration.buildGraphConfiguration())
                .locking(true)
                .keyOrdered(true)
                .supportsInterruption(false)
                .optimisticLocking(false)
                .build();
        //  .scanTxConfig(GraphDatabaseConfiguration.buildGraphConfiguration()
        //     .set(ISOLATION_LEVEL, IsolationLevel.READ_UNCOMMITTED.toString()))
            //.supportsInterruption(false)
               // .optimisticLocking(false)
                //.build();

//        features = new StoreFeatures();
//        features.supportsOrderedScan = true;
//        features.supportsUnorderedScan = false;
//        features.supportsBatchMutation = false;
//        features.supportsTxIsolation = transactional;
//        features.supportsConsistentKeyOperations = true;
//        features.supportsLocking = true;
//        features.isKeyOrdered = true;
//        features.isDistributed = false;
//        features.hasLocalKeyPartition = false;
//        features.supportsMultiQuery = false;
    }

    /**review this
     private void initialize(int cachePercent) throws BackendException {
     try {
     EnvironmentConfig envConfig = new EnvironmentConfig();
     envConfig.setAllowCreate(true);
     envConfig.setTransactional(transactional);
     envConfig.setCachePercent(cachePercent);

     if (batchLoading) {
     envConfig.setConfigParam(EnvironmentConfig.ENV_RUN_CHECKPOINTER, "false");
     envConfig.setConfigParam(EnvironmentConfig.ENV_RUN_CLEANER, "false");
     }

     //Open the environment
     environment = new Environment(directory, envConfig);


     } catch (DatabaseException e) {
     throw new PermanentBackendException("Error during Snowflake initialization: ", e);
     }

     }**/

    @Override
    public StoreFeatures getFeatures() {
        return features;
    }

    @Override
    public List<KeyRange> getLocalKeyPartition() throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SnowFlakeTransaction beginTransaction(final BaseTransactionConfig txCfg) throws BackendException {
        if (tx == null) {
            tx = new SnowFlakeTransaction(txCfg, this);
            if (isLogged) {
                log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
            }
        }

        return tx;
    }
////
//        Configuration effectiveCfg =
//                new MergedConfiguration(txCfg.getCustomOptions(), txCfg.getCustomOptions());//, getStorageConfig());
//
////            if (transactional) {
////                TransactionConfig txnConfig = new TransactionConfig();
////                ConfigOption.getEnumValue(effectiveCfg.get(ISOLATION_LEVEL),IsolationLevel.class).configure(txnConfig);
////                tx = environment.beginTransaction(null, txnConfig);
////            }
//        tx = new SnowFlakeTransaction(txCfg, this);
//
//        if (isLogged) {
//            log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
//        }
//        return tx;
//    }

    @Override
    public SnowFlakeKeyValueStore openDatabase(String name) throws BackendException {
        Preconditions.checkNotNull(name);
        if (stores.containsKey(name)) {
            return stores.get(name);
        }
//        try {
//            DatabaseConfig dbConfig = new DatabaseConfig();
//            dbConfig.setReadOnly(false);
//            dbConfig.setAllowCreate(true);
//            dbConfig.setTransactional(transactional);
//
//            dbConfig.setKeyPrefixing(true);
//
//            if (batchLoading) {
//                dbConfig.setDeferredWrite(true);
//            }

            //  Database db = environment.openDatabase(null, name, dbConfig);
            SnowFlakeDatabase db = null;
            try{
                db = new SnowFlakeDatabase(name, this.getTransaction());///change it to transaction as required.
            }
            catch (IOException e) {
                if (isLogged)
                    log.severe(LogUtils.logTraceback("Couldn't connect to snowflake as log file creation failed", e));
                else
                    throw new PermanentBackendException("Couldn't connect to snowflake due to log file creation failure");
            }
            catch (SnowFlakeConnectionException e) {
                if (isLogged)
                    log.severe("Couldn't connect to snowflake due to connection error");
                else
                    throw new PermanentBackendException("Couldn't connect to snowflake due to connection error");
            }

            databases.put(name, db);

            if (isLogged)
                log.info(LogUtils.stringFormatter("Opened database %s", name));

            SnowFlakeKeyValueStore store = null;
            try{
                store = new SnowFlakeKeyValueStore(name, this);
            }
            catch (SQLException e) {
                log.severe("Couldn't connect to snowflake store due to invalid query");
            }
            catch (SnowFlakeConnectionException e) {
                log.severe("Couldn't connect to snowflake store");
            }

            stores.put(name, store);
            return store;
//        } catch (DatabaseException e) {
//            throw new PermanentBackendException("Could not open SnowFlake data store", e);
//        }
    }

    @Override
    public void mutateMany(Map<String, KVMutation> mutations, StoreTransaction txh) throws BackendException {
        for (Map.Entry<String,KVMutation> mutation : mutations.entrySet()) {
            SnowFlakeKeyValueStore store = openDatabase(mutation.getKey());
            KVMutation mutationValue = mutation.getValue();

            if (!mutationValue.hasAdditions() && !mutationValue.hasDeletions()) {
                log.warning(LogUtils.stringFormatter("Empty mutation set for %s, doing nothing", mutation.getKey()));
            } else {
                log.warning(LogUtils.stringFormatter("Mutating %s", mutation.getKey()));
            }

            if (mutationValue.hasAdditions()) {
                for (KeyValueEntry entry : mutationValue.getAdditions()) {
                    store.insert(entry.getKey(),entry.getValue(),txh);
                    log.info(LogUtils.stringFormatter("Insertion on %s: %s", mutation.getKey(), entry.toString()));
                }
            }
            if (mutationValue.hasDeletions()) {
                for (StaticBuffer del : mutationValue.getDeletions()) {
                    store.delete(del,txh);
                    log.info(LogUtils.stringFormatter("Deletion on %s: %s", mutation.getKey(), del.toString()));
                }
            }
        }
    }

    void removeDatabase(SnowFlakeKeyValueStore db) {
        if (!stores.containsKey(db.getName())) {
            throw new IllegalArgumentException("Tried to remove an unkown database from the storage manager");
        }
        String name = db.getName();
        stores.remove(name);
        log.info(LogUtils.stringFormatter("Removed database %s", name));
    }

    ExecutorService getExecutorService() {
        return this.executorService;
    }

    @Override
    public void close() {

        databases.forEach((name, database) -> {
            try {
                database.close();
                log.info("Closed " + name);
            } catch (PermanentBackendException e) {
                log.severe(LogUtils.stringFormatter("Error closing %s ", name));
            }
        });

        stores.forEach( (name, store) -> {
            try {
                store.close();
                log.info(LogUtils.stringFormatter("Closed store %s ", name));
            } catch (BackendException e) {
                log.severe(LogUtils.stringFormatter("Error closing connection to store %s ", name));
            }
        });

        this.tx.close();
        this.executorService.shutdownNow();
//
////        if (environment != null) {
//            if (!stores.isEmpty())
//                throw new IllegalStateException("Cannot shutdown manager since some databases are still open with stores " + stores.toString());
//            try {
//                // TODO this looks like a race condition
//                //Wait just a little bit before closing so that independent transaction threads can clean up.
//                Thread.sleep(30);
//            } catch (InterruptedException e) {
//                //Ignore
//            }
//            try{
//                this.close();
//            } catch (BackendException e) {
//                throw new PermanentBackendException("Could not close Snowflake database", e);
//            }

//            try {
//                environment.close();
//            } catch (DatabaseException e) {
//                throw new PermanentBackendException("Could not close Snowflake database", e);
//            }
//        }
    }

//    private static final Transaction NULL_TRANSACTION = null;

    @Override
    public void clearStorage() throws BackendException {
        if (!stores.isEmpty()) {
            throw new IllegalStateException("Cannot delete store, since database is open: " + stores.keySet().toString());
        }
        //list the tables from snowflake db and remove the respective table..
        // - in snowflake db - 2 functions, list table and remove table.
        SnowFlakeDatabase db = null;
        try{
            db = new SnowFlakeDatabase("", this.getTransaction());///change it to transaction as required.

            OperationStatus tableList = db.getTables("db", "schema");

            if (tableList.getStatus()) {
                List<String> tables = (List<String>) tableList.getResult();

                for (final String dbName :tables)
                {
                    OperationStatus status = db.removeTable("schema", dbName);
                    if (status.getStatus()) {
                        log.info(LogUtils.stringFormatter("Removed database %s (clearStorage)", db.getTableName()));
                    }
                    else {
                        log.severe(LogUtils.stringFormatter("Couldn't remove table %s from snowflake backend", dbName));
                    }
                }
            }
            else{
                throw new PermanentBackendException("Unable to fetch table list from snowflake due to error");
            }
        }
        catch (IOException e) {
            log.severe("Couldnt connect to snowflake as log file couldn't be created");
        }
        catch (SnowFlakeConnectionException e) {
            log.severe("Couldnt connect to snowflake");
        }
        close();
        // IOUtils.deleteFromDirectory(directory);
    }

    @Override
    public boolean exists() throws BackendException {
//        return !environment.getDatabaseNames().isEmpty();
        // check table names not empty here..
        return true;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName() ;//+ ":" + directory.toString();
    }

//
//    public enum IsolationLevel {
//        READ_UNCOMMITTED {
//            @Override
//            void configure(TransactionConfig cfg) {
//                cfg.setReadUncommitted(true);
//            }
//        }, READ_COMMITTED {
//            @Override
//            void configure(TransactionConfig cfg) {
//                cfg.setReadCommitted(true);
//
//            }
//        }, REPEATABLE_READ {
//            @Override
//            void configure(TransactionConfig cfg) {
//                // This is the default and has no setter
//            }
//        }, SERIALIZABLE {
//            @Override
//            void configure(TransactionConfig cfg) {
//                cfg.setSerializableIsolation(true);
//            }
//        };
//
//        abstract void configure(TransactionConfig cfg);
//    }

    private static class TransactionBegin extends Exception {
        private static final long serialVersionUID = 1L;

        private TransactionBegin(String msg) {
            super(msg);
        }
    }

    SnowFlakeTransaction getTransaction() throws BackendException {
        if (tx == null) {
            tx = new SnowFlakeTransaction(cfg, this);

            if (isLogged) {
                log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
            }

        }

        return tx;
    }
}
