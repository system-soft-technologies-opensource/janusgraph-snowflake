// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.diskstorage.snowflake;


import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeCursor;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.*;

import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVQuery;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeySelector;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStore;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.BackendException;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import org.janusgraph.diskstorage.util.RecordIterator;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
//import org.slf4j.Logger;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;


public class SnowFlakeKeyValueStore implements OrderedKeyValueStore{

//    private static final Logger log = LoggerFactory.getLogger(SnowFlakeKeyValueStore.class);
    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeKeyValueStore.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private SingletonLogger logger = SingletonLogger.getInstance("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private static Logger log = null;
    private boolean isLogged = true;

    private final String name;
    private final SnowFlakeStoreManager storeManager;
    private SnowFlakeDatabase db;
    private boolean isOpen;

    private static final StaticBuffer.Factory<byte[]> ENTRY_FACTORY = (array, offset, limit) -> {
        final byte[] bArray = new byte[limit - offset];
        System.arraycopy(array, offset, bArray, 0, limit - offset);
        return bArray;
    };

    public SnowFlakeKeyValueStore(String tableName, SnowFlakeStoreManager manager) throws SQLException, SnowFlakeConnectionException, BackendException {
        try {
            log = logger.getLogger();
        } catch (IOException e) {
            System.out.println("Unable to create logger class. Not going to use logger and will do sysout");
            isLogged = false;
        }

//        log = logger.getLogger();

        name = tableName;
        storeManager = manager;
        try {
            this.initializeDatabase();
        } catch (IOException e){
            throw new PermanentBackendException("Unable to create connection to SnowFlake as log file creation failed");
        }
    }

    private void initializeDatabase() throws IOException, SnowFlakeConnectionException, BackendException {
        SnowFlakeTransaction tx = this.storeManager.getTransaction();

        db = new SnowFlakeDatabase(name, tx);

        if (isLogged) {
            log.info(String.format("Initialized database with db = %s, tx = %s, name = %s", db.getTableName(), tx, name));
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh) throws BackendException {
        insert(key, value, txh, true);
    }
    //constructor of  --- insert and get ...
// get operation from table system_properties, key value, to_binary ...decode to base64
    // base64 string to decode in to byte array
    //byte array to static buffer.
    
    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh, boolean allowOverwrite) throws BackendException {

        try {
            OperationStatus status;

            if (isLogged) {
                log.info(String.format("db=%s, op=insert, tx=%s", name, txh));
            }

            byte[] keyByte = key.as(ENTRY_FACTORY);
            byte[] valueByte = value.as(ENTRY_FACTORY);

            if (allowOverwrite)
                status = db.put(keyByte, valueByte);
            else
                status = db.putNotOverWrite(keyByte, valueByte);

            if (!status.getStatus()) {
                if (status.KEYEXIST) {
                    throw new PermanentBackendException("Key already exists on no-overwrite.");
                } else {
                    throw new PermanentBackendException("Could not write entity, return status: " + status);
                }
            }
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public RecordIterator<KeyValueEntry> getSlice(KVQuery query, StoreTransaction txh) throws BackendException {
        if (isLogged) {
            log.info(String.format("db=%s, op=getSlice, tx=%s", name, txh));
        }
//        final Transaction tx = getTransaction(txh);
        final StaticBuffer keyStart = query.getStart();
        final StaticBuffer keyEnd = query.getEnd();
        final KeySelector selector = query.getKeySelector();

        final List<KeyValueEntry> result = new ArrayList<>();
        final byte[] foundKey = keyStart.as(ENTRY_FACTORY);
//        final byte[] foundKey = null;
//        final DatabaseEntry foundData = new DatabaseEntry();
//        final byte[] foundData = null;
        SnowFlakeCursor cursor;

        try {
            cursor = db.openCursor();
        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException("Unable to open Cursor as Log file creation failed");
        }

//        try (final Cursor cursor = db.openCursor(tx, null)) {
//            OperationStatus status = cursor.getSearchKeyRange(foundKey, foundData, getLockMode(txh));
//            //Iterate until given condition is satisfied or end of records
//            while (status.getStatus()) {
//                StaticBuffer key = getBuffer(foundKey);
//
//                if (key.compareTo(keyEnd) >= 0)
//                    break;
//
//                if (selector.include(key)) {
//                    result.add(new KeyValueEntry(key, getBuffer(foundData)));
//                }
//
//                if (selector.reachedLimit())
//                    break;
//
//                status = cursor.getNext(foundKey, foundData, getLockMode(txh));
//            }
//        } catch (Exception e) {
//            throw new PermanentBackendException(e);
//        }
        OperationStatus status = new OperationStatus(true);
        //Iterate until given condition is satisfied or end of records

        ResultSet res;

        res = cursor.getSearchKey(foundKey);

        if (res == null) {
            throw new PermanentBackendException("Unable to find the key " + foundKey +
                    " and " + keyStart.toString() + " from tbl " + db.getTableName());
        }

        try {
            while (res.next()) {

                StaticBuffer key = getBuffer(foundKey);

                if (key.compareTo(keyEnd) >= 0)
                    break;

                if (selector.include(key)) {
                    String encodedData = res.getString("VALUE");

                    byte[] foundData = Base64.getDecoder().decode(encodedData);

                    result.add(new KeyValueEntry(key, getBuffer(foundData)));
                }

                if (selector.reachedLimit())
                    break;

//            status = cursor.getNext(foundKey, foundData, getLockMode(txh));
            }
        }
        catch (SQLException e) {
            throw new PermanentBackendException("Invalid iteration over records");
        }

//        log.trace("beginning db={}, op=getSlice, tx={}", name, txh);
//        final StaticBuffer keyStart = query.getStart();
//        final StaticBuffer keyEnd = query.getEnd();
//        final KeySelector selector = query.getKeySelector();
//        final List<KeyValueEntry> result = new ArrayList<>();
//        final byte[] foundKey = db.pack(keyStart.as(ENTRY_FACTORY));
//        final byte[] endKey = db.pack(keyEnd.as(ENTRY_FACTORY));
//
//        try {
//            final List<KeyValue> results = tx.getRange(foundKey, endKey, query.getLimit());
//
//            for (final KeyValue keyValue : results) {
//                StaticBuffer key = getBuffer(db.unpack(keyValue.getKey()).getBytes(0));
//                if (selector.include(key))
//                    result.add(new KeyValueEntry(key, getBuffer(keyValue.getValue())));
//            }
//        } catch (Exception e) {
//            throw new PermanentBackendException(e);
//        }
//
//        log.trace("db={}, op=getSlice, tx={}, resultcount={}", name, txh, result.size());

        if (isLogged) {
            log.info(String.format("db=%s, op=getSlice, key=%s, tx=%s, resultcount=%s", name,
                    Base64.getEncoder().encodeToString(foundKey), txh, result.size()));
        }

//        log.trace("db={}, op=getSlice, tx={}, resultcount={}", name, txh, result.size());

        return new RecordIterator<KeyValueEntry>() {
            private final Iterator<KeyValueEntry> entries = result.iterator();

            @Override
            public boolean hasNext() {
                return entries.hasNext();
            }

            @Override
            public KeyValueEntry next() {
                return entries.next();
            }

            @Override
            public void close() {
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public Map<KVQuery,RecordIterator<KeyValueEntry>> getSlices(List<KVQuery> queries, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(StaticBuffer key, StoreTransaction txh) throws BackendException {

        if (isLogged) {
            log.info(String.format("db=%s, op=delete, tx=%s, key=%s", name, txh, key.toString()));
        }

        OperationStatus status;
        try {
            if (isLogged) {
                log.info(String.format("db=%s, op=deleteSuccess, tx=%s, key=%s", name, txh, key.toString()));
            }

            byte[] keyByte = key.as(ENTRY_FACTORY);

            status = db.delete(keyByte);

            if (!status.getStatus()) {
                throw new PermanentBackendException("Could not remove: " + status);
            }
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public StaticBuffer get(StaticBuffer key, StoreTransaction txh) throws BackendException {
        try {
//            DatabaseEntry databaseKey = key.as(ENTRY_FACTORY);
//            DatabaseEntry data = new DatabaseEntry();

            if (isLogged) {
                log.info(String.format("db=%s, op=get, tx=%s, key=%s", name, txh, key.toString()));
            }

            // Convert StaticBuffer to byte array
            byte[] keyByte = key.as(ENTRY_FACTORY);

            // Encode byte array to Base64.
            String encodedkey = Base64.getEncoder().encodeToString(keyByte);

            // Do DB Operation using Base64 Key.
            OperationStatus status = db.get(encodedkey);
//
            if (status.getStatus()) {
                // ArrayList result = status.getResultAsList("KEY", "VALUE").get(0);
                ArrayList<String> result = status.getResultAsList("KEY");
                String dbEncodedString = result.get(0);

                // Decode the return Base64 String to byte array
                byte[] entry = Base64.getDecoder().decode(dbEncodedString);
                //byte[] entry = encodedEntry.getBytes();

                System.out.println(entry.toString());
//            final byte[] entry = null;

                // Convert Byte Array to StaticBuffer.
                return getBuffer(entry);
            }
            else
                return getBuffer(null);


//            // ArrayList result = status.getResultAsList("KEY", "VALUE").get(0);
//            ArrayList<String> result = status.getResultAsList("KEY");
//            String dbEncodedString = result.get(0);
//
//            // Decode the return Base64 String to byte array
//            byte[] entry = Base64.getDecoder().decode(dbEncodedString);
//            //byte[] entry = encodedEntry.getBytes();
//
//            System.out.println(entry.toString());
////            final byte[] entry = null;
//
//            // Convert Byte Array to StaticBuffer.
//            return getBuffer(entry);
//           // System.out.println(status.getStatus());
//
//           /* if (status.getStatus()) {
////                final byte[] entry = status.getResult();
//                String encodedEntry =  status.getResult().toString();
//                System.out.println(encodedEntry);
//                System.out.println(encodedEntry);
//                byte[] entry = Base64.getDecoder().decode(encodedEntry);
//                //byte[] entry = encodedEntry.getBytes();
//                System.out.println(entry.toString());
////                final byte[] entry = null;
//                return getBuffer(entry);
//            } else {
//            	System.out.println(" Return null");
//                return null;
//            }*/
        } catch (Exception e) {
        	e.printStackTrace();
            throw new PermanentBackendException(e);
        }
//        ArrayList arr = new ArrayList<Byte>(10);
//        return StaticArrayBuffer(arr, 0, 0);
    }

    @Override
    public boolean containsKey(StaticBuffer key, StoreTransaction txh) throws BackendException {
        return get(key,txh)!=null;
    }

    @Override
    public void acquireLock(StaticBuffer key, StaticBuffer expectedValue, StoreTransaction txh) throws BackendException {
        if (this.storeManager.getTransaction() == null) {
            if (isLogged) {
                log.warning("Attempt to acquire lock with transactions disabled");
            }
        } //else we need no locking
    }

    @Override
    public synchronized void close() throws BackendException {
        try {
            if(isOpen) db.close();
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
//        if (isOpen) storeManager.removeDatabase(this);
        isOpen = false;
    }

    private static StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

//    private static StaticBuffer getBuffer(DatabaseEntry entry) {
//        return new StaticArrayBuffer(entry.getData(),entry.getOffset(),entry.getOffset()+entry.getSize());
//    }
}
