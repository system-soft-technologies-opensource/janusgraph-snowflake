// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnectionManager;
import com.sstech.janusgraph.common.utils.JanusGraphSnowFlakeLogger;
//import com.sstech.janusgraph.common.utils.LogMe;
import com.sstech.janusgraph.common.utils.LogUtils;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.common.AbstractStoreTransaction;

import java.io.IOException;
import java.util.logging.Logger;


public class SnowFlakeTransaction  extends AbstractStoreTransaction {

    private final SnowFlakeStoreManager storeManager;
    private SnowFlakeConnectionManager connectionManager = SnowFlakeConnectionManager.getInstance(3000);

    private String URL = "";
    private String userName = "";
    private String pwd = "";
    private String warehouse = "";
    private String DB = "";
    private String schema = "";
    private String role = "";

    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger(SnowFlakeTransaction.class);
//    private JanusGraphSnowFlakeLogger logger = new JanusGraphSnowFlakeLogger("/var/log/custom.log");
//    private static SingleFileLogger logger = SingleFileLogger.getInstance("/var/log/janusgraph-snowflake/custom.log");
    private Logger log;

    public SnowFlakeTransaction(final BaseTransactionConfig config, SnowFlakeStoreManager manager) throws PermanentBackendException{
        super(config);
        this.storeManager = manager;
//        log = logger.getLogger();
        try {
            log = logger.getLogger();
        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException(LogUtils.stringFormatter("Unable to create log file %s while executing", logger.getLogFileName()));
        }

        setConnectionParameters();
        try {
            connectToSnowFlake();
        }
        catch (SnowFlakeConnectionException e) {
            e.printStackTrace();
            throw new PermanentBackendException("Couldn't connect to SnowFlake DB with properties URL: " + URL +
                    " user: " + userName + " password: " + pwd + " warehouse: " + warehouse + " role: " + role + " DB: " + DB + " schema: " + schema);
        }

       

//        try {
//            connectionManager.setPassword("");
//        }
//        catch (SnowFlakeConnectionException e) {
//            throw new PermanentBackendException("Error creating connection to SnowFlake");
//        }
    }

    private void setConnectionParameters() {
        // Manjunath's credentials
//        URL = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com";
//        userName = "Manjunath";
//        warehouse = "COMPUTE_WH";
//        pwd = "Access234!";
//        DB = "GRAPHDBTEST";
//        schema = "GRAPHDBTEST";
//        role = "SYSADMIN";
        // Debasish
        URL = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com";
        userName = "Debasish";
        warehouse = "COMPUTE_WH";
        pwd = "Nano789!";
        DB = "GRAPHDBTEST";
        schema = "GRAPHDBTEST1";
        role = "SYSADMIN";

        log.info(LogUtils.stringFormatter("Set SnowFlake connection parameters as URL=%s, userName=%s, " +
                "warehouse=%s, password=%s, DB=%s, schema=%s, role=%s", URL, userName, warehouse, pwd, DB, schema, role));

    }

    private void connectToSnowFlake() throws SnowFlakeConnectionException{
        connectionManager.setURL(URL);
        connectionManager.setUserName(userName);
        connectionManager.setPassword(pwd);
        connectionManager.setWarehouse(warehouse);
        connectionManager.setDB(DB);
        connectionManager.setSchema(schema);
        connectionManager.setRole(role);

        connectionManager.connect();

        log.info("Connected to SnowFlakeConnectionManager class with specified configurations");
    }

    public SnowFlakeConnection getConnection() throws SnowFlakeConnectionException {
        log.info("Successfully retrieved connection from SnowFlakeConnectionManager.");
        return connectionManager.getConnection();
    }

    public void close() {
        connectionManager.close();
    }

}
