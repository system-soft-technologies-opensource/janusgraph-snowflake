package com.sstech.janusgraph.examples;

import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.common.AbstractStoreTransaction;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class SnowFlakeTransaction  extends AbstractStoreTransaction {

    private final SnowFlakeStoreManager storeManager;

    public SnowFlakeTransaction(final BaseTransactionConfig config, SnowFlakeStoreManager manager) {
        super(config);
        this.storeManager = manager;
    }

    public void execute(String statement) {

    }

    //connect or initialize - to connect to db snowflake..

    private static Connection getConnection()
            throws SQLException
    {
        try
        {
            Class.forName("com.snowflake.client.jdbc.SnowflakeDriver");
        }
        catch (ClassNotFoundException ex)
        {
            System.err.println("Driver not found");
        }
        // build connection properties
        Properties properties = new Properties();
        properties.put("user", "Manjunath");     // replace "" with your username
        properties.put("password", ""); // replace "" with your password
        // properties.put("account", "ik55883.east-us-2.azure");  // replace "" with your account name
        properties.put("warehouse", "COMPUTE_WH");
        properties.put("db", "GRAPHDBTEST");       // replace "" with target database name
        properties.put("schema", "GRAPHDBTEST");   // replace "" with target schema name
        properties.put("role", "SYSADMIN");
        //properties.put("tracing", "on");
        // https://ik55883.east-us-2.azure.snowflakecomputing.com/console

        // create a new connection
        String connectStr = System.getenv("SF_JDBC_CONNECT_STRING");
        // use the default connection string if it is not set in environment
        if(connectStr == null)
        {
            connectStr = "jdbc:snowflake://ik55883.east-us-2.azure.snowflakecomputing.com"; // replace accountName with your account name
        }
        return DriverManager.getConnection(connectStr, properties);
    }

}
